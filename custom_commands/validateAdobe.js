exports.command = function(pageDataValues, attribute) {
  var self = this;
  var dataValues = pageDataValues[attribute];
  this.execute(
    function(data) {
        var iterateObject = function (object, container) {
          if (typeof(object)=== 'undefined' || typeof(container) === 'undefined')
              return 'Undefined Error';
          // Contains the JS object that we retrieve the values i.e. tealium_s, data layer ect.
          var pageContainer = container;

          var tempResult = true;

          for (var key in object) {
              if (object.hasOwnProperty(key)) {
                  if (object[key].type === 'value') {
                      if (pageContainer[key] !== object[key].value) {
                        tempResult = 'Error: ' + key + ' - ' + object[key].value + ' - ' + pageContainer[key];
                        break;
                      }
                  } else if (object[key].type === 'type'){
                    if (typeof(pageContainer[key]) !== object[key].value) {
                      result = 'Error: ' + key + ' - ' + object[key].value + ' - ' + pageContainer[key];
                      break;
                    }
                  } else if (object[key].type === 'regex') {
                    var regex = new RegExp(object[key].value, "i");
                    //var regex = object[key].value;
                    if (pageContainer[key].match(regex)) {
                        // Do nothing
                    } else {
                        tempResult = 'Error: ' + key + ' - ' + object[key].value + ' - ' + pageContainer[key];
                        break;
                    }
                  } else {
                      // Generic Error
                      tempResult = 'Mismatch Error';
                      break;
                  }
              }
          }

          return tempResult;
        }

        var result = true;
        // In-page objects
        var sObject = window.tealium_s;
        var pageDataLayer = window.utag_data;

        // Validation objects defined for testing
        var pageData = data.main;
        var commonData = data.common.analytics;
        var dl = data.datalayer;

        var result1 = iterateObject(pageData, sObject);
        var result2 = iterateObject(commonData, sObject);
        var result3 = iterateObject(dl, pageDataLayer);

        if (result1 !== true)
            result = result1;
        else if (result2 !== true)
            result = result2;
        else if (result3 !== true)
            result = result3;

        //alert(result);
        return result;
      }, [ {'main' : dataValues.analytics, 'common' : pageDataValues.common , 'datalayer' : dataValues.datalayer}],
      function(data){
          this.verify.ok( data.value === true, attribute );
      }
  );

  return this; // allows the command to be chained.
};
