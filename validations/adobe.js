var homeData = {};
homeData.analytics = {};
homeData.datalayer = {};
homeData.analytics.pageName = { 'type' : 'value' , 'value' : 'home+page:DEB_HP01' };
homeData.analytics.server = { 'type' : 'value' , 'value' : 'www.debenhams.com' };
homeData.analytics.eVar37 = { 'type' : 'type' , 'value' : 'string' };
// Data Layer
homeData.datalayer.page_section = { 'type' : 'value' , 'value' : 'home+page' };

var tcatData = {};
tcatData.analytics = {};
tcatData.datalayer = {};
tcatData.analytics.pageName = { 'type' : 'value' , 'value' : 'Women:DEB_TCAT' };
tcatData.analytics.server = { 'type' : 'value' , 'value' : 'www.debenhams.com' };
tcatData.analytics.eVar37 = { 'type' : 'type' , 'value' : 'string' };
// Click Map Value
//tcatData.prop51 = { 'type' : 'value' , 'value' : 'home+page:DEB_HP01- TopNav:Women'}
// Data Layer
tcatData.datalayer.page_section = { 'type' : 'value' , 'value' : 'Women' };

var pspData = {};
pspData.analytics = {};
pspData.datalayer = {};
pspData.analytics.pageName = { 'type' : 'value' , 'value' : 'Women:Dresses:DEB_PROD01' };
pspData.analytics.server = { 'type' : 'value' , 'value' : 'www.debenhams.com' };
pspData.analytics.prop1 = { 'type' : 'value' , 'value' : 'Product Selection'};
pspData.analytics.prop7 = { 'type' : 'value' , 'value' : 'Dresses'};
pspData.analytics.prop13 = { 'type' : 'value' , 'value' : '/women/dresses'};
//pspData.prop16 = { 'type' : 'regex' , 'value' : '/\d*/'};
pspData.analytics.prop32 = { 'type' : 'type' , 'value' : 'string' };
pspData.analytics.eVar37 = { 'type' : 'type' , 'value' : 'string' };
pspData.analytics.prop39 = { 'type' : 'value' , 'value' : 'en'};
pspData.analytics.prop40 = { 'type' : 'value' , 'value' : 'GB'};
pspData.analytics.prop46 = { 'type' : 'value' , 'value' : 'Product Selection'};
// Data Layer
tcatData.datalayer.page_section = { 'type' : 'value' , 'value' : 'Women' };

var pdpData = {};
pdpData.analytics = {};
pdpData.datalayer = {};
pdpData.analytics.pageName = { 'type' : 'value' , 'value' : 'Women:Dresses:Casual+dresses:DEB_PROD02'};
pdpData.analytics.products = { 'type' : 'regex' , 'value' : "^(;\\d*;;;event23=\\d*\\|event92=\\d*;evar33=\\d*\\|evar62=dropdown\\|evar61=Product Detail\\|evar52=\\d*\\|evar20=Direct Access\\|evar50=STD-NDH-NEVE-NOMDH-NOMEVE-CFS-INT,(?:;\\d*;;;event92=\\d*;evar33=\\d*\\|evar62=dropdown\\|evar61=Product Detail\\|evar52=\\d*\\|evar20=Direct Access\\|evar50=STD-NDH-NEVE-NOMDH-NOMEVE-CFS-INT,?)*)$"}
pdpData.datalayer.page_section = { 'type' : 'value' , 'value' : 'Women' };

var commonData = {};
commonData.analytics = {};
commonData.datalayer = {};
commonData.analytics.eVar39 = { 'type' : 'regex' , 'value' : "\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d" };
commonData.analytics.currencyCode = { 'type' : 'value' , 'value' : 'GBP' };

var addToCart = {};
addToCart.analytics = {};
addToCart.datalayer = {};
addToCart.analytics.events = { 'type' : 'value' , 'value' : 'scAdd,event7,scOpen'};

var allData = {
    'Home' : homeData,
    'Top Level Category' : tcatData,
    'PSP' : pspData,
    'PDP' : pdpData,
    'common' : commonData,
    'Add to Cart' : addToCart
};

module.exports = allData;
