var data = require('./../validations/adobe.js');

module.exports = {
  'Home' : function (browser) {
    browser
      .url('http://www.debenhams.com/')
      .pause(2000)
      .validateAdobe( data, 'Home' )
      //.click('li.menu-l1-li a.menu-l1-link-title')
      //.pause(2000);
      //.validateAdobe( data, 'Top Level Category')
      //.click('ul.navlistCls_s li a[href=/women/dresses]')
      //.end();
  },
  'TCAT' : function (browser) {
    browser
      .url('http://www.debenhams.com/women')
      .pause(2000)
      .validateAdobe( data , 'Top Level Category' );
      //.click('ul.navlistCls_s li a[href=/women/dresses]')
      //.end();
  },
  'PSP' : function (browser) {
    browser
    .url('http://www.debenhams.com/women/dresses')
    .pause(2000)
    .validateAdobe( data, 'PSP' )
    .end();
  },
  'PDP' : function (browser) {
    browser
    .url('http://www.debenhams.com/webapp/wcs/stores/servlet/prod_10701_10001_120010661258_-1')
    //.maximizeWindow()
    .pause(2000)
    .validateAdobe( data , 'PDP' )
    .pause(1000)
    .click('ul li.size-selection-option:first-child')
    .pause(1000)
    .click('div.alter-arrows a.alter-up-arrow')
    .pause(1000)
    .click('a#productPageAdd2Cart')
    .pause(1000)
    .validateAdobe( data , 'Add to Cart')
    .pause (500)
    /*.click('div.main-header-item.main-mini-bag-bt button.button')
    .pause(1000)
    .click('div.alter-arrows a.alter-up-arrow')
    .pause(1000)
    .click('div.primary a#secure-checkout-proceed-buttons')
    .pause(1000)
    .setValue('input#WC_CheckoutLogon_FormInput_logonId','test@test.com')
    .pause(1000)
    .click('input#new_customer')
    .pause(1000)
    .click('div a#continue_button')
    .pause(1000)
    .click('div#CheckoutLogonDiv > div > div > a:last-child')
    .pause(1000)
    .click('div#ChooseHome')*/
    .end();
  }
};
